This is your new Play application
=================================

This file will be packaged with your application, when using `activator dist`.

To start the application first run activator console. Then

``` import play.api._ ```

``` val env = Environment(new java.io.File("."), this.getClass.getClassLoader, Mode.Dev) ```

```val context = ApplicationLoader.createContext(env)```

```val loader = ApplicationLoader(context)```

```val app = loader.load(context)```

```Play.start(app)```

```import Play.current```

```import Command._```

```Command.initDB()```

The api uses HTTP basic auth with customer/driver id as username and authToken as password. Use postman to send requests

Booking process
A customer requests a cab with a location and an optional car type
The request is logged in the `requests` table
If a cab is found then is allocated and and the entry in `requests` table is marked accepted and the `cab` is marked assigned in the `cabs` table
A notification to driver is sent with the customer location and request id (Not implemented/Wasnt required)
The driver picks up the customer and sends a pickup confirmation. This inserts a new entry in the `rides` table
After dropping the customer the driver sends a drop confirmation. This marks the `ride` entry in `rides` table as accepted, logs the fare
of the ride, and marks the cab unassigned
The `payments` table is used for logging received payments. (Not implemented/Wasnt required)
