package models

import org.joda.time.DateTime
import utils.PgAccess
import scala.slick.driver.PostgresDriver.simple._

// Cab requests
case class CabRequest(id: Option[Int]=None,
                   customerId: Int,
                   fromLocation: String,  // is in the form `<lat>,<long>` eg. 22,45
                   acceptedWithCabId: Option[Int], // when a cab is found for the request then its value is the cab id. None otherwise
                   acceptedWithDriverId: Option[Int], // when a cab is found for the request then its value is the cab's current driver id. None otherwise
                   madeAt: DateTime     // utc date-time of request
                     ) extends PgAccess {

  def insert: CabRequest = {
    databasePool withSession {
      implicit session =>
        val id = (CabRequest.requestTable returning CabRequest.requestTable.map(_.id)) += this
        this.copy(id = Some(id))
    }
  }

  def accept(cabId: Int, driverId: Int): Unit = {
    databasePool withSession {
      implicit session =>
        CabRequest.requestTable.filter(_.id === this.id.get).map(cr=> (cr.acceptedWithCabId, cr.acceptedWithDriverId))
          .update((Some(cabId), Some(driverId)))
    }
  }
}

object CabRequest extends PgAccess {
  def requestTable = TableQuery[tables.CabRequestTable]

  def findById(id: Int): Option[CabRequest] = {
    databasePool withSession {
      implicit session =>
        requestTable.filter(_.id === id).firstOption

    }
  }
}