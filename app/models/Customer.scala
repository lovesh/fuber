package models

import utils.PgAccess
import scala.slick.driver.PostgresDriver.simple._


case class Customer(id: Option[Int]=None,
                                      name: String,
                                      contactNo: String,
                                      authToken: String      // TODO: Should be a password and should have an authenticating mechanism
                                       ) extends PgAccess {
  def insert: Customer = {
    databasePool withSession {
      implicit session =>
        val id = (Customer.customerTable returning Customer.customerTable.map(_.id)) += this
        this.copy(id = Some(id))
    }
  }
}

object Customer extends PgAccess {
  def customerTable = TableQuery[tables.CustomerTable]

  def authenticateById(id: Int, authToken: String): Option[Customer] = {
    databasePool withSession {
      implicit session =>
        val customer = customerTable.filter(c=> c.id === id && c.authToken === authToken)
        customer.firstOption
    }
  }
}