package models

import org.joda.time.DateTime
import utils.PgAccess
import scala.slick.driver.PostgresDriver.simple._

case class Payment(id: Option[Int]=None,
                     rideId: Int,
                     amountPaid: Option[Double],    // this is None until the payment is made, when done its value is the payment made by customer
                     paidAt: Option[DateTime]       // this is None until the payment is made, when done its value is the utc date-time of payment made by customer
                     ) extends PgAccess {

  def insert: Payment = {
    databasePool withSession {
      implicit session =>
        val id = (Payment.paymentTable returning Payment.paymentTable.map(_.id)) += this
        this.copy(id = Some(id))
    }
  }
}

object Payment extends PgAccess {
  def paymentTable = TableQuery[tables.PaymentTable]
}

