package models

import utils.PgAccess
import scala.slick.driver.PostgresDriver.simple._

// Car types like pink car, white car, black car. White car is the normal car with 0 extra cost

case class CabType(id: Option[Int]=None,
                   name: String,
                    extraCost: Int) extends PgAccess {

  def insert: CabType = {
    databasePool withSession {
      implicit session =>
        val id = (CabType.cabTypeTable returning CabType.cabTypeTable.map(_.id)) += this
        this.copy(id = Some(id))
    }
  }
}


object CabType extends PgAccess {
  def cabTypeTable = TableQuery[tables.CabTypeTable]
  val defaultCabTypeName = "white-car"

  def findById(id: Int): Option[CabType] = {
    databasePool withSession {
      implicit session =>
        cabTypeTable.filter(_.id === id).firstOption

    }
  }
}