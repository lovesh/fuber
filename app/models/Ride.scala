package models

import org.joda.time.{DateTimeZone, DateTime}
import utils.PgAccess
import scala.slick.driver.PostgresDriver.simple._

case class Ride(id: Option[Int]=None,
                fromLocation: String,       // is in the form `<lat>,<long>` eg. 22,45, can be different from `Request`'s location
                toLocation: Option[String],     // this is None until the ride is complete, when complete its value is drop location
                fromTime: DateTime,
                 toTime: Option[DateTime],      // this is None until the ride is complete, when complete its value is drop time
                requestId: Int,
                isComplete: Boolean,        // is the ride going on
                fare: Option[Double],          // this is None until the ride is complete, when complete its value is the fare
                extraInfo: Option[String]   // any extra info about the ride, like driver/customer feedback etc
                  ) extends PgAccess {

  def insert: Ride = {
    databasePool withSession {
      implicit session =>
        val id = (Ride.rideTable returning Ride.rideTable.map(_.id)) += this
        this.copy(id = Some(id))
    }
  }

  def completed(atLocation: String): Double = {
    val unow = DateTime.now(DateTimeZone.UTC)
    databasePool withSession {
      implicit session =>
        val q = for {
          cr <- CabRequest.requestTable if cr.id === this.requestId
          c <- Cab.cabTable if cr.acceptedWithCabId === c.id
        } yield c
        val cab = q.firstOption.get
        cab.unassign
        cab.updateLocation(atLocation)
        val fare = Ride.calculateFare(this.fromLocation, atLocation, this.fromTime, unow, cab.typeId)
        Ride.rideTable.filter(_.id === id).map(r=> (r.toLocation, r.toTime, r.isComplete, r.fare)).update((Option(atLocation), Option(unow), true, Option(fare)))
        fare
    }
  }
}

object Ride extends PgAccess {
  def rideTable = TableQuery[tables.RideTable]

  def findById(id: Int): Option[Ride] = {
    databasePool withSession {
      implicit session =>
        rideTable.filter(_.id === id).firstOption

    }
  }

  def calculateFare(fromLocation: String, toLocation: String, fromTime: DateTime, toTime: DateTime, cabTypeId: Int): Double = {
    val extraCost = CabType.findById(cabTypeId).get.extraCost
    val Array(x, y) = fromLocation.split(",").map(_.toLong)
    val Array(x1, y1) = toLocation.split(",").map(_.toLong)
    val distance = Math.sqrt(Math.pow((x-x1), 2) + Math.sqrt(Math.pow((y-y1), 2)))
    (distance*2) + (toTime.getMillis - fromTime.getMillis)*1/(60*1000) + extraCost
  }
}