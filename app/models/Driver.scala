package models

import utils.PgAccess
import scala.slick.driver.PostgresDriver.simple._

case class Driver(id: Option[Int]=None,
                   name: String,
                   contactNo: String,
                   authToken: String      // TODO: Should be a password and should have an authenticating mechanism
                   ) extends PgAccess {

  def insert: Driver = {
    databasePool withSession {
      implicit session =>
        val id = (Driver.driverTable returning Driver.driverTable.map(_.id)) += this
        this.copy(id = Some(id))
    }
  }
}

object Driver extends PgAccess {
  def driverTable = TableQuery[tables.DriverTable]

  def authenticateById(id: Int, authToken: String): Option[Driver] = {
    databasePool withSession {
      implicit session =>
        val driver = driverTable.filter(d=> d.id === id && d.authToken === authToken)
        driver.firstOption
    }
  }
}