package models.tables

import org.joda.time.DateTime
import scala.slick.driver.PostgresDriver.simple._
import models.Payment
import com.github.tototoshi.slick.PostgresJodaSupport._


class PaymentTable(tag: Tag) extends Table[Payment](tag, "payments") {
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def rideId = column[Int]("rideId")
  def amountPaid = column[Option[Double]]("amount_paid")
  def paidAt = column[Option[DateTime]]("paid_at")

  val allF = (id.?, rideId, amountPaid, paidAt)
  type allT = (Option[Int], Int, Option[Double], Option[DateTime])

  def * = allF <> ({ p: allT =>
    (Payment.apply _).tupled(p)
  }, { p: Payment =>
    Some((p.id, p.rideId, p.amountPaid, p.paidAt))
  })

  def ride = foreignKey("RIDE_FK", rideId, TableQuery[RideTable])(_.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)
}
