package models.tables

import models.Customer
import org.joda.time._
import scala.slick.driver.PostgresDriver.simple._

class CustomerTable(tag: Tag) extends Table[Customer](tag, "customers") {
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def name = column[String]("name")
  def contactNo = column[String]("contact_no")
  def authToken = column[String]("auth_token")

  val allF = (id.?, name, contactNo, authToken)
  type allT = (Option[Int], String, String, String)

  def * = allF <> ({ c: allT =>
    (Customer.apply _).tupled(c)
  }, { c: Customer =>
    Some((c.id, c.name, c.contactNo, c.authToken))
  })

  def idxContactNo = index("CUSTOMER_CONTACT_NO", contactNo, unique = true)
}
