package models.tables

import models.CabType
import org.joda.time._
import scala.slick.driver.PostgresDriver.simple._


class CabTypeTable(tag: Tag) extends Table[CabType](tag, "cab_types") {
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def name = column[String]("name")
  def extraCost = column[Int]("extra_cost", O.Default(1))

  val allF = (id.?, name, extraCost)
  type allT = (Option[Int], String, Int)

  def * = allF <> ({ c: allT =>
    (CabType.apply _).tupled(c)
  }, { c: CabType =>
    Some((c.id, c.name, c.extraCost))
  })
}
