package models.tables

import org.joda.time._
import scala.slick.driver.PostgresDriver.simple._
import models.Cab


class CabTable(tag: Tag) extends Table[Cab](tag, "cabs") {
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def vehicleNo = column[String]("cab_no")
  def typeId = column[Int]("type_id")
  def parkedLocation = column[String]("parked_location")
  def currentDriverId = column[Int]("currentDriverId")
  def isAssigned = column[Boolean]("is_assigned", O.Default(false))

  val allF = (id.?, vehicleNo, typeId, parkedLocation, currentDriverId, isAssigned)
  type allT = (Option[Int], String, Int, String, Int, Boolean)

  def * = allF <> ({ c: allT =>
    (Cab.apply _).tupled(c)
  }, { c: Cab =>
    Some((c.id, c.vehicleNo, c.typeId, c.parkedLocation, c.currentDriverId, c.isAssigned))
  })

  def `type` = foreignKey("TYPE_FK", typeId, TableQuery[CabTypeTable])(_.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)
  def currentDriver = foreignKey("CURRENT_DRIVER_FK", currentDriverId, TableQuery[DriverTable])(_.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)
  def idxVehicleNo = index("CAB_VEHICLE_NO", vehicleNo, unique = true)
}
