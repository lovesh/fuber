package models.tables

import models.Driver
import org.joda.time._
import scala.slick.driver.PostgresDriver.simple._


class DriverTable(tag: Tag) extends Table[Driver](tag, "drivers") {
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def name = column[String]("name")
  def contactNo = column[String]("contact_no")
  def authToken = column[String]("auth_token")

  val allF = (id.?, name, contactNo, authToken)
  type allT = (Option[Int], String, String, String)

  def * = allF <> ({ d: allT =>
    (Driver.apply _).tupled(d)
  }, { d: Driver =>
    Some((d.id, d.name, d.contactNo, d.authToken))
  })

  def idxContactNo = index("DRIVER_CONTACT_NO", contactNo, unique = true)
}
