package models.tables

import org.joda.time.DateTime
import scala.slick.driver.PostgresDriver.simple._
import scala.slick.ast.ColumnOption.DBType
import models.Ride
import com.github.tototoshi.slick.PostgresJodaSupport._


class RideTable(tag: Tag) extends Table[Ride](tag, "rides") {
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def fromLocation = column[String]("from_location")
  def toLocation = column[Option[String]]("to_location")
  def fromTime = column[DateTime]("from_time")
  def toTime = column[Option[DateTime]]("to_time")
  def requestId = column[Int]("request_id")
  def isComplete = column[Boolean]("is_complete", O.Default(false))
  def fare = column[Option[Double]]("fare")
  def extraInfo = column[Option[String]]("extra_info", DBType("text"))

  val allF = (id.?, fromLocation, toLocation, fromTime, toTime, requestId, isComplete, fare, extraInfo)
  type allT = (Option[Int], String, Option[String], DateTime, Option[DateTime], Int, Boolean, Option[Double], Option[String])

  def * = allF <> ({ r: allT =>
    (Ride.apply _).tupled(r)
  }, { r: Ride =>
    Some((r.id, r.fromLocation, r.toLocation, r.fromTime, r.toTime, r.requestId, r.isComplete, r.fare, r.extraInfo))
  })

  def request = foreignKey("REQUEST_FK", requestId, TableQuery[CabRequestTable])(_.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)

}
