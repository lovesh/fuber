package models.tables

import org.joda.time._
import scala.slick.driver.PostgresDriver.simple._
import models.CabRequest
import com.github.tototoshi.slick.PostgresJodaSupport._


class CabRequestTable(tag: Tag) extends Table[CabRequest](tag, "requests") {
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def customerId = column[Int]("customer_id")
  def fromLocation = column[String]("from_location")
  def acceptedWithCabId = column[Option[Int]]("cab_id")
  def acceptedWithDriverId = column[Option[Int]]("driver_id")
  def madeAt = column[DateTime]("made_at")
  val allF = (id.?, customerId, fromLocation, acceptedWithCabId, acceptedWithDriverId, madeAt)
  type allT = (Option[Int], Int, String, Option[Int], Option[Int], DateTime)

  def * = allF <> ({ r: allT =>
    (CabRequest.apply _).tupled(r)
  }, { r: CabRequest =>
    Some((r.id, r.customerId, r.fromLocation, r.acceptedWithCabId, r.acceptedWithDriverId, r.madeAt))
  })

  def customer = foreignKey("CUSTOMER_FK", customerId, TableQuery[CustomerTable])(_.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)
  def driver = foreignKey("DRIVER_FK", acceptedWithDriverId, TableQuery[DriverTable])(_.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)
  def cab = foreignKey("CAB_FK", acceptedWithCabId, TableQuery[CabTable])(_.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)
}

