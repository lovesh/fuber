package models

import utils.PgAccess
import scala.slick.driver.PostgresDriver.simple._


case class Cab(id: Option[Int]=None,
                vehicleNo: String,
                typeId: Int,
                parkedLocation:String,    // is in the form `<lat>,<long>` eg. 22,45
                currentDriverId: Int,     // the current driver of the cab, because drivers of a cab can change
                isAssigned: Boolean       // true when the cab is assigned to someone. false otherwise
                ) extends PgAccess {

  def insert: Cab = {
    databasePool withSession {
      implicit session =>
        val id = (Cab.cabTable returning Cab.cabTable.map(_.id)) += this
        this.copy(id = Some(id))
    }
  }

  def assign: Unit = {
    databasePool withSession {
      implicit session =>
        Cab.cabTable.filter(_.id === this.id.get).map(_.isAssigned).update(true)
    }
  }

  def unassign: Unit = {
    databasePool withSession {
      implicit session =>
        Cab.cabTable.filter(_.id === this.id.get).map(_.isAssigned).update(false)
    }
  }

  def updateLocation(location: String): Unit = {
    databasePool withSession {
      implicit session =>
        Cab.cabTable.filter(_.id === this.id).map(_.parkedLocation).update(location)
    }
  }
}

object Cab extends PgAccess {
  def cabTable = TableQuery[tables.CabTable]

  def get(location: String, cabTypeId: Option[Int]): Option[Cab] = {
    val Array(x, y) = location.split(",").map(_.toLong)

    val cabs = databasePool withSession {
      implicit session =>
        cabTypeId match {
          case Some(id: Int) =>
            cabTable.filter(c=> c.typeId === id && c.isAssigned === false).list
          case _ =>
            val defaultCabType = CabType.cabTypeTable.filter(_.name === CabType.defaultCabTypeName).first
            // Wont allocate pink cab or any other special cab even if its nearest because someone else might need it.
            // TODO: Consider this choice again
            cabTable.filter(c=> c.typeId === defaultCabType.id.get && c.isAssigned === false).list
        }
    }


    var minDistance: Long = Long.MaxValue
    var allocatedCab: Cab = null

    // for each cab check if its distance from current location is less than the minimum distance. Its not very efficient. Can use K-D Tree
    cabs.foreach { c =>
      val Array(x1, y1) = c.parkedLocation.split(",").map(_.toLong)
      if (Math.sqrt(Math.pow((x-x1), 2) + Math.sqrt(Math.pow((y-y1), 2))) < minDistance)
        allocatedCab = c
    }

    Option(allocatedCab)
  }

  def findById(id: Int): Option[Cab] = {
    databasePool withSession {
      implicit session =>
        cabTable.filter(_.id === id).firstOption

    }
  }
}