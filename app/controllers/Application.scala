package controllers

import models._
import org.joda.time.{DateTime, DateTimeZone}
import play.api._
import play.api.libs.json.Json
import play.api.mvc._
import play.mvc.Http

import scala.concurrent.Future
import org.apache.commons.codec.binary.Base64

import scala.util.matching.Regex
import scala.util.matching.Regex.Match
import scala.concurrent.ExecutionContext.Implicits.global

class Application extends Controller {

  def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }

  // all this auth functionality can be put in a base controller trait which is extended by all controllers

  def getBasicAuthParams(headers: Headers): Option[(String, String)] = {
    headers.get(Http.HeaderNames.AUTHORIZATION) match {
      case Some(header) =>
        val BasicHeader = "Basic (.*)".r
        header match {
          case BasicHeader(base64) =>
            try {
              val decodedBytes = Base64.decodeBase64(base64.getBytes)
              val credentials = new String(decodedBytes).split(":", 2)
              if (credentials.length != 2) {
                None
              } else {
                val (user, password) = (credentials(0), credentials(1))
                Some((user, password))
              }
            } // TODO: Catch an exception here and take an appropriate action

          case _ => None
        }

      case _ => None
    }
  }

  class AuthenticatedCustomerRequest[T](val customerId: Int, val request: Request[T]) extends WrappedRequest[T](request)

  object AuthenticatedCustomerAction extends ActionBuilder[AuthenticatedCustomerRequest] with ActionRefiner[Request, AuthenticatedCustomerRequest] {

    def refine[T](request: Request[T]): Future[Either[Result, AuthenticatedCustomerRequest[T]]] = {
      getBasicAuthParams(request.headers) match {
        case Some(authParams: (String, String)) =>
          Customer.authenticateById(authParams._1.toInt, authParams._2) match {
            case Some(c: Customer) =>
              Future(Right(new AuthenticatedCustomerRequest[T](authParams._1.toInt, request)))
            case _ =>
              Future(Left(Forbidden("Auth token does not match")))
          }
        case _ =>
          Future(Left(Forbidden("No auth token sent")))
      }
    }
  }

  class AuthenticatedDriverRequest[T](val driverId: Int, val request: Request[T]) extends WrappedRequest[T](request)

  object AuthenticatedDriverAction extends ActionBuilder[AuthenticatedDriverRequest] with ActionRefiner[Request, AuthenticatedDriverRequest] {

    def refine[T](request: Request[T]): Future[Either[Result, AuthenticatedDriverRequest[T]]] = {
      getBasicAuthParams(request.headers) match {
        case Some(authParams: (String, String)) =>
          Driver.authenticateById(authParams._1.toInt, authParams._2) match {
            case Some(d: Driver) =>
              Future(Right(new AuthenticatedDriverRequest[T](authParams._1.toInt, request)))
            case _ =>
              Future(Left(Forbidden("Auth token does not match")))
          }
        case _ =>
          Future(Left(Forbidden("No auth token sent")))
      }
    }
  }

  val locationPattern = new Regex("^[0-9.]+,[0-9.]+$")

  def getCab(location: String, cabTypeId: Option[Int]) = AuthenticatedCustomerAction.async { implicit request =>
    locationPattern.findFirstMatchIn(location) match {
      case Some(m: Match) => Unit
      case _ => Future.successful(BadRequest(Json.toJson(Json.obj(
        "success"-> false,
        "message"-> "location not in proper format"
      ))))
    }

    val cabRequest = CabRequest(None, request.customerId, location, None, None, DateTime.now(DateTimeZone.UTC)).insert
    Cab.get(location, cabTypeId) match {
      case Some(cab: Cab) =>
        cabRequest.accept(cab.id.get, cab.currentDriverId)
        cab.assign

        // TODO: A message needs to be sent to the driver to pick up the customer with his location and request id. Would use a notification service
        // Read the `Readme.md` for understanding the pickup process

        // A more verbose response could have been sent that has driver contact details and cab details
        Future.successful(Ok(Json.toJson(Json.obj(
          "success"-> true,
          "message"-> "got cab",
          "cabId"-> cab.id.get,
          "cabRequestId"-> cabRequest.id.get
        ))))
      case _ =>
        Future.successful(Ok(Json.toJson(Json.obj(
          "success"-> false,
          "message"-> "no cab"
        ))))
    }
  }

  // this should be a post method making it a post method would make me write the form mapping which would take some time
  // TODO: Convert to a post method
  def pickedUpCustomer(cabRequestId: Int, fromLocation: String) = AuthenticatedDriverAction.async { implicit request =>
    // location being taken as a parameter because location might have changed between request and pickup
    locationPattern.findFirstMatchIn(fromLocation) match {
      case Some(m: Match) => Unit
      case _ => Future.successful(BadRequest(Json.toJson(Json.obj(
        "success"-> false,
        "message"-> "location not in proper format"
      ))))
    }

    CabRequest.findById(cabRequestId) match {
      case Some(cr: CabRequest) =>
        val ride = Ride(None, fromLocation, None, DateTime.now(DateTimeZone.UTC), None, cr.id.get, false, None, None).insert
        Cab.findById(cr.acceptedWithCabId.get).get.updateLocation(fromLocation)

        Future.successful(Ok(Json.toJson(Json.obj(
          "success"-> true,
          "message"-> "ride starting",
          "rideId"-> ride.id.get
        ))))
      case _ =>
        Future.successful(BadRequest(Json.toJson(Json.obj(
          "success"-> false,
          "message"-> "invalid request id"
        ))))
    }
  }

  // this should be a post method making it a post method would make me write the form mapping which would take some time
  // TODO: Convert to a post method
  def droppedCustomer(rideId: Int, toLocation: String) = AuthenticatedDriverAction.async { implicit request =>
    // drop time could have been taken from the driver because there might be delay between the drop and drop report. no internet, etc
    locationPattern.findFirstMatchIn(toLocation) match {
      case Some(m: Match) => Unit
      case _ => Future.successful(BadRequest(Json.toJson(Json.obj(
        "success"-> false,
        "message"-> "location not in proper format"
      ))))
    }

    Ride.findById(rideId) match {
      case Some(ride: Ride) =>
        val fare = ride.completed(toLocation)
        Future.successful(BadRequest(Json.toJson(Json.obj(
          "success"-> true,
          "message"-> "ride completed",
          "fare"-> fare
        ))))
      case _ =>
        Future.successful(BadRequest(Json.toJson(Json.obj(
          "success"-> false,
          "message"-> "invalid ride id"
        ))))
    }


  }
}
