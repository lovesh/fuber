import models.{Customer, Driver, Cab, CabType}
import models.tables._
import org.postgresql.util.PSQLException
import utils.PgAccess
import scala.slick.driver.PostgresDriver.simple._


object Command extends PgAccess {

  def createTables(): Unit = {
    databasePool withSession {
      implicit session =>
        try {
          (TableQuery[CabTypeTable].ddl
            ++ TableQuery[DriverTable].ddl
            ++ TableQuery[CustomerTable].ddl
            ++ TableQuery[CabTable].ddl
            ++ TableQuery[CabRequestTable].ddl
            ++ TableQuery[RideTable].ddl
            ++ TableQuery[PaymentTable].ddl
            ).create
        }
        catch {
          case e: PSQLException => println(e.getMessage)
        }
    }
  }

  def initDB(): Unit = {
    createTables()

    // insert drivers
    val d1 = Driver(None, "d1", "999", "191").insert
    val d2 = Driver(None, "d2", "888", "181").insert
    val d3 = Driver(None, "d3", "777", "171").insert
    val d4 = Driver(None, "d4", "666", "161").insert
    val d5 = Driver(None, "d5", "555", "151").insert

    // insert customers
    val cu1 = Customer(None, "c1", "111", "111")
    cu1.insert
    val cu2 = Customer(None, "c2", "222", "121")
    cu2.insert
    val cu3 = Customer(None, "c3", "333", "131")
    cu3.insert
    val cu4 = Customer(None, "c4", "444", "141")
    cu4.insert
    val cu5 = Customer(None, "c5", "000", "151")
    cu5.insert

    // insert cab types, pink car, white car, etc
    val ct1 = CabType(None, CabType.defaultCabTypeName, 0).insert
    val ct2 = CabType(None, "pink-car", 5).insert

    // insert cabs
    val ca1 = Cab(None, "123", ct1.id.get, "0,0", d1.id.get, false)
    ca1.insert
    val ca2 = Cab(None, "900", ct1.id.get, "0,0", d2.id.get, false)
    ca2.insert
    val ca3 = Cab(None, "910", ct1.id.get, "0,0", d3.id.get, false)
    ca3.insert
    val ca4 = Cab(None, "300", ct2.id.get, "0,0", d4.id.get, false)
    ca4.insert
    val ca5 = Cab(None, "d00", ct2.id.get, "0,0", d5.id.get, false)
    ca5.insert
  }

  def dropTables() {
    databasePool withSession {
      implicit session =>
        try {
          (TableQuery[CabTypeTable].ddl
            ++ TableQuery[DriverTable].ddl
            ++ TableQuery[CustomerTable].ddl
            ++ TableQuery[CabTable].ddl
            ++ TableQuery[CabRequestTable].ddl
            ++ TableQuery[RideTable].ddl
            ++ TableQuery[PaymentTable].ddl
            ).drop
        }
        catch {
          case e: PSQLException => println(e.getMessage)
        }
    }
  }
}
