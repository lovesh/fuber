package utils

import com.mchange.v2.c3p0.ComboPooledDataSource
import play.api.Play.current
import slick.driver.PostgresDriver
import scala.slick.driver.PostgresDriver.simple._


object PgPool {
  var pool: PostgresDriver.backend.DatabaseDef = null

  def getPool(driver: String, connectionUrl: String): PostgresDriver.backend.DatabaseDef = {
    if (pool == null)
      pool = {
        val ds = new ComboPooledDataSource
        ds.setDriverClass(driver)
        ds.setJdbcUrl(connectionUrl)
        ds.setMinPoolSize(current.configuration.getInt("postgres.minPoolSize").get)
        ds.setAcquireIncrement(current.configuration.getInt("postgres.acquireIncrement").get)
        ds.setMaxPoolSize(current.configuration.getInt("postgres.maxPoolSize").get)
        Database.forDataSource(ds)
      }
    pool
  }
}

trait PgAccess {
  val connectionUrl: String = {
    current.configuration.getString("db.default.url").get +
      "?user=" +
      current.configuration.getString("db.default.user").get +
      "&password=" +
      current.configuration.getString("db.default.password").get
  }
  val driver: String = current.configuration.getString("db.default.driver").get

  //val database = Database.forURL(connectionUrl, driver = driver)

  lazy val databasePool = PgPool.getPool(driver, connectionUrl)
}

